<?php

namespace App\Form;

use App\Entity\Tag;
use App\Data\DataSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SearchForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder,array $options)
    {
        $builder->add("q",TextType::class,[
                      "label" => false,
                      "required" => false,
                      "attr" => ["placeholder" =>"Rechercher"]])

                ->add("tags",EntityType::class,[
                      "class" => Tag::class,
                      "label" => "false",
                      "required" => false,
                      "expanded" => true,
                      "multiple" => true])

                ->add("minprice",NumberType::class,[
                      "label" => false,
                      "required" => false,
                      "attr" => ["placeholder" =>"Prix minimum"]])

                ->add("maxprice",NumberType::class,[
                      "label" => false,
                      "required" => false,
                      "attr" => ["placeholder" =>"Prix maximum"]])
                      
                ->add("promotion",CheckboxType::class,[
                      "label" => "En promotion",
                      "required" => false])
        ;
                
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => DataSearch::class,
            "method" => "GET",
            "csrf_protection" => false
        ]);
    }

    public function getBlockPrefix()
    {
        return "";
    }
}