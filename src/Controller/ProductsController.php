<?php 

namespace App\Controller;

use App\Data\DataSearch;
use App\Form\SearchForm;
use App\Repository\ProductRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductsController extends AbstractController
{
    private $_repository;

    public function __construct(ProductRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * @Route("/products",name="products")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
       $data = new DataSearch();
       $data->setPage($request->get("page",1));
       $createform = $this->createForm(SearchForm::class,$data);
       $createform->handleRequest($request);
       $form = $createform->createView();
       $products = $this->_repository->findSearch($data);
       return $this->render("product/index.html.twig",compact("products","form"));
    }

    /**
     * @Route("/rss",name="rss")
     * @return Response
     */
    public function rss()
    {
        $products = $this->_repository->findAll();
        $encoders = [new XmlEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $content = [];
        foreach($products as $product) {

            $tags = $product->getTags()->getValues();
            $tagarray = [];

            foreach($tags as $tag){
                $tagarray[$tag->getId()] = $tag->getName();
            }

            $prodarray = [
                "id" => $product->getId(),
                "name" => $product->getName(),
                "price" => $product->getPrice(),
                "description" => $product->getDescription(),
                "content" => $product->getContent(),
                "image" => $product->getImage(),
                "promotion" => $product->getPromotion(),
                "count_tags" => $product->getTags()->count(),
                "tag" => $tagarray
            ];

            array_push($content, $prodarray);
        }
        
        $rssfeed = $serializer->serialize($content, 'xml');

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml;charset=UTF-8');
        $response->setContent($rssfeed);

        return $response;

    }
    
}