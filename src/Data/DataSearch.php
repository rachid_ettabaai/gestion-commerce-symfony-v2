<?php

namespace App\Data;

use App\Entity\Tag;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class DataSearch
{   
    /**
     * @var integer
     */
    private $page = 1;

    /**
     * @var string
     */
    private $q;

    /**
     * @var Collection|Tag[]
     */
    private $tags;

    /**
     * @var null|integer
     */
    private $maxprice;

    /**
     * @var null|integer
     */
    private $minprice;

    /**
     * @var boolean
     */
    private $promotion = false;

    public function __construct() {
        
        $this->tags = new ArrayCollection();
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage(int $page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * Get the value of q
     *
     * @return  string
     */ 
    public function getQ()
    {
        return $this->q;
    }

    /**
     * Set the value of q
     *
     * @param  string  $q
     *
     * @return  self
     */ 
    public function setQ(string $q)
    {
        $this->q = $q;

        return $this;
    }

    /**
     * Set the value of tags
     *
     * @param  Tag  $tags
     *
     * @return  self
     */
    public function setTags(Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * Get the value of maxprice
     *
     * @return  null|integer
     */ 
    public function getMaxprice()
    {
        return $this->maxprice;
    }

    /**
     * Set the value of maxprice
     *
     * @param  null|integer  $maxprice
     *
     * @return  self
     */ 
    public function setMaxprice($maxprice)
    {
        $this->maxprice = $maxprice;

        return $this;
    }

    /**
     * Get the value of minprice
     *
     * @return  null|integer
     */ 
    public function getMinprice()
    {
        return $this->minprice;
    }

    /**
     * Set the value of minprice
     *
     * @param  null|integer  $minprice
     *
     * @return  self
     */ 
    public function setMinprice($minprice)
    {
        $this->minprice = $minprice;

        return $this;
    }

    /**
     * Get the value of promotion
     *
     * @return  boolean
     */ 
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * Set the value of promotion
     *
     * @param  boolean  $promotion
     *
     * @return  self
     */ 
    public function setPromotion(bool $promotion)
    {
        $this->promotion = $promotion;

        return $this;
    }

}