<?php

namespace App\Repository;

use App\Entity\Product;
use App\Data\DataSearch;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry,PaginatorInterface $paginator)
    {
        parent::__construct($registry, Product::class);
        $this->paginator = $paginator;
    }

    /**
     * Get products linking a search 
     * @return PaginationInterface
     */
    public function findSearch(DataSearch $dataSearch) : PaginationInterface
    {
        $query = $this->createQueryBuilder("p")->select("tags","p")->join("p.tags","tags");

        if(!empty($dataSearch->getQ())){
            $query->andWhere("p.name LIKE :q")->setParameter("q","%{$dataSearch->getQ()}%");
        }

        if($dataSearch->getMinprice()){
            $query->andWhere("p.price >= :minprice")->setParameter("minprice", $dataSearch->getMinprice());
        }

        if($dataSearch->getMaxprice()){
            $query->andWhere("p.price <= :maxprice")->setParameter("maxprice", $dataSearch->getMaxprice());
        }

        if($dataSearch->getPromotion()){
            $query->andWhere("p.promotion = 1");
        }

        if(!$dataSearch->getTags()->isEmpty()){
            $query = $query->andWhere("tags.id IN (:tags)")->setParameter("tags", $dataSearch->getTags());
        }

        $query = $query->getQuery();

        return $this->paginator->paginate(
            $query,
            $dataSearch->getPage(),
            10
        );

    }

}
