import '../css/app.css';
import $ from 'jquery';
import Materialize from 'materialize-css/dist/js/materialize.min.js';

$(document).ready(function() {
    Materialize.Sidenav.init($('.sidenav'));
});