Brief Gestion
======================

# Présentation:

Gestion des produits

# Techno utilisés:

symfony + webpack encore

# Installation:

* composer install
* npm install
* bin/console doctrine:database.create
* bin/console make:migration
* bin/console doctrine:make:migrate
* bin/console hautelook:fixtures:load
* php -S localhost:8080 -t public/